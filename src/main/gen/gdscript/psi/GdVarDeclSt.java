// This is a generated file. Not intended for manual editing.
package gdscript.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface GdVarDeclSt extends GdStmt {

  @Nullable
  GdAssignTyped getAssignTyped();

  @Nullable
  GdExpr getExpr();

  @Nullable
  GdTyped getTyped();

  @NotNull
  GdVarNmi getVarNmi();

  @NotNull
  String getName();

  @NotNull
  String getReturnType();

}
